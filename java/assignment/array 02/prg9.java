/*

   Program 9
Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]

Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array

*/

import java.io.*;

class commEle {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a size of first array: ");
		int size1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[size1];
		
		System.out.println("Enter a size of Second array: ");
		int size2 = Integer.parseInt(br.readLine());
		int arr2[] = new int[size2];


		System.out.println("Enter first array elements: ");
		for(int i = 0 ; i < arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		
		}
		
		System.out.println("Enter Second array elements: ");
		for(int i = 0 ; i < arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}
		
		int arr3[] = new int[size1 + size2];

		for(int i = 0; i < arr3.length; i++){
			if(i < size1){
				arr3[i] = arr1[i];
			}else{
				arr3[i] = arr2[i - size1];
			}
		}

		System.out.print("New third array is : ");
		for (int i = 0; i < arr3.length; i++){
			System.out.print(arr3[i]+" ");
		}
		System.out.println();
	}

}

