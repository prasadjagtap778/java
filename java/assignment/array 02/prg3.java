/*

Program 3
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26

*/

import java.io.*;

class sumEvenOdd {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a size of array: ");

		int size = Integer.parseInt(br.readLine());
		int evensum = 0, oddsum = 0;
		int arr[] = new int[size];
		System.out.println("Enter array elements: ");
		for(int i = 0 ; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if (arr[i] % 2 == 0){
				evensum = evensum + arr[i];
			}else{
				oddsum = oddsum + arr[i];
			}
		}
		System.out.println("Sum of even elements is : "+evensum);
		System.out.println("Sum of odd elements is : "+oddsum);
	}
}


			



