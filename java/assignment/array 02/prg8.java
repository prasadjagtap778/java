/*

   Program 8
WAP to find the uncommon elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Uncommon elements :
3
5
9
8

*/

import java.io.*;

class uncommEle {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a size of first array: ");
		int size1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[size1];
		
		System.out.println("Enter a size of Second array: ");
		int size2 = Integer.parseInt(br.readLine());
		int arr2[] = new int[size2];


		System.out.println("Enter first array elements: ");
		for(int i = 0 ; i < arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		
		}
		
		System.out.println("Enter Second array elements: ");
		for(int i = 0 ; i < arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.print("Uncommon number Between two arrays is : ");
		for(int i = 0 ; i < arr1.length; i++){
			int flag = 0;
			for (int j = 0 ; j < arr2.length; j++){
			
				if (arr1[i] == arr2[j]){
					flag = 1;
				}
			}
			if(flag == 0){
				System.out.print(arr1[i]+" ");
			}
		}
		System.out.println();
		
		for(int i = 0 ; i < arr2.length; i++){
			int flag = 0;
			for (int j = 0 ; j < arr1.length; j++){
			
				if (arr2[i] == arr1[j]){
					flag = 1;
				}
			}
			if(flag == 0){
				System.out.print(arr2[i]+" ");
			}
		}
		System.out.println();
			
	}
}
