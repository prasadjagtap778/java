/*

Program 7
WAP to find a Strong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5

*/

import java.io.*;

class strongNum {
	 public int numStrong(int arr[]){
		for(int i = 0; i < arr.length; i++){
			int num = arr[i];
		
			int sum = 0;
			while(num != 0){
				int fact = 1;
				int temp = num % 10;
				for(int j = 1; j <= temp; j++){
					fact = fact * j;
				}
				sum = sum + fact;
				num = num / 10;
			}
			if(arr[i] == sum){
				return i;
			}
		}
		System.out.println();
		return -1;

	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		strongNum obj = new strongNum();
		int index = obj.numStrong(arr);
		if (index == -1){
			System.out.print("Strong number not found in array");
		}else{
			System.out.print("Strong number "+arr[index]+" found at index "+index);
		}
		System.out.println();
	}
}
