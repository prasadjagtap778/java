/*

Program 5
WAP to find a Perfect number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3

*/

import java.io.*;

class perfectEle {
	 public int elePerfect(int arr[]){
		for(int i = 0; i < arr.length; i++){
			int num = arr[i];
			int sum = 0;
			for(int j = 1; j < arr[i]; j++){
				if(arr[i] % j == 0)
					sum = sum + j;
				
			}
			
			if(arr[i] == sum)
				return i;
			
		
		}
		System.out.println();
		return -1;

	}



	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		perfectEle obj = new perfectEle();
		int index = obj.elePerfect(arr);
		if (index == -1){
			System.out.print("Perfect number not found in array");
		}else{
			System.out.print("Perfect number "+arr[index]+" found at index "+index);
		}
		System.out.println();
	}
}
