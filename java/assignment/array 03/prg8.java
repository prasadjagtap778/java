/*

Program 8
WAP to find an ArmStong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4

*/

import java.io.*;

class amstrongNum {
	 public int numAmstrong(int arr[]){
		for(int i = 0; i < arr.length; i++){
			
			int num = arr[i];
			int count = 0;
			int sum = 0;
			while(num != 0){
				count++;
				num = num / 10;
			}
			int num2 = arr[i];
			while(num2 != 0){
				int temp = num2 % 10;
				int mul = 1;
				for(int j = 0; j < count; j++){
					mul = mul * temp;
					
				}
				sum = sum + mul;
				num2 = num2 / 10;
			}


			if(arr[i] == sum){
				return i;
			}
		}
		System.out.println();
		return -1;

	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		amstrongNum obj = new amstrongNum();
		int index = obj.numAmstrong(arr);
		if (index == -1){
			System.out.print("Amstrong number not found in array");
		}else{
			System.out.print("Amstrong number "+arr[index]+" found at index "+index);
		}
		System.out.println();
	}
}
