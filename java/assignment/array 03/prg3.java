/*

Program 3
WAP to nd a composite number from an array and return its index.
Take size and elements from the user
Input: 1 2 3 5 6 7
Output: composite 6 found at index: 4

*/

import java.io.*;

class compositeEle {
	 public int eleCompo(int arr[]){
		for(int i = 0; i < arr.length; i++){
			int num = arr[i];
			int count = 0;
			for(int j = 1; j <= arr[i]; j++){
				if(arr[i] % j == 0)
					count++;
				
			}
			
			if(count > 2)
				return i;
			
		System.out.println();
		}
		System.out.println();
		return -1;

	}



	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		compositeEle obj = new compositeEle();
		int index = obj.eleCompo(arr);
		if (index == -1){
			System.out.print("Composite number not found in array");
		}else{
			System.out.print("Composite number "+arr[index]+" found at index "+index);
		}
		System.out.println();
	}
}
