/*

Program 4
WAP to find a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5

*/

import java.io.*;

class primeEle {
	 public int elePrime(int arr[]){
		for(int i = 0; i < arr.length; i++){
			int num = arr[i];
			int count = 0;
			for(int j = 1; j <= arr[i]; j++){
				if(arr[i] % j == 0)
					count++;
				
			}
			
			if(count == 2)
				return i;
			
		
		}
		System.out.println();
		return -1;

	}



	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		primeEle obj = new primeEle();
		int index = obj.elePrime(arr);
		if (index == -1){
			System.out.print("Prime number not found in array");
		}else{
			System.out.print("Prime number "+arr[index]+" found at index "+index);
		}
		System.out.println();
	}
}
