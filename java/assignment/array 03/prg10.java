/*

Program 10
Write a program to print the second min element in the array
Input: Enter array elements: 255 2 1554 15 65 95 89
Output: 15

*/
import java.io.*;

class secondMinEle {
	 int eleSecondMin(int arr[]){
		int min = arr[0];
		for(int i = 1; i < arr.length; i++){
			if(arr[i] < min){
				min = arr[i];
			}
		}
		int max = arr[0];
		for(int i = 1; i < arr.length; i++){
			if(arr[i] > max){
				max = arr[i];
			}
		}
		int smin = max;
		for(int i = 1; i < arr.length; i++){
			if (arr[i] < smin && arr[i] > min){
				smin = arr[i];
			}
		}
		return smin;

	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		secondMinEle obj = new secondMinEle();
		int smin = obj.eleSecondMin(arr);
		System.out.println("Second min element from array is: "+smin);
	}
}
