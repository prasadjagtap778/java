/*

Program 6
WAP to find a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564
Output: Palindrome no 252 found at index: 2

*/

import java.io.*;

class palindromeEle {
	 public int elePalindrome(int arr[]){
		for(int i = 0; i < arr.length; i++){
			int num = arr[i];
			int rev = 0;
			while(num != 0){
				int temp = num % 10;
				rev = (rev * 10) + temp;
				num = num / 10;
			}
			if(arr[i] == rev)
				return i;
		}
		System.out.println();
		return -1;

	}



	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		palindromeEle obj = new palindromeEle();
		int index = obj.elePalindrome(arr);
		if (index == -1){
			System.out.print("Palindrome number not found in array");
		}else{
			System.out.print("Palindrome number "+arr[index]+" found at index "+index);
		}
		System.out.println();
	}
}
