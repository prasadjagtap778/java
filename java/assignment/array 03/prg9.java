/*

Program 9
Write a program to print the second max element in the array
Input: Enter array elements: 2 255 2 1554 15 65
Output: 255

*/

import java.io.*;

class secondMaxEle {
	 int eleSecondMax(int arr[]){
		int max = arr[0];
		for(int i = 1; i < arr.length; i++){
			if(arr[i] > max){
				max = arr[i];
			}
		}
		int smax = 0;
		for(int i = 1; i < arr.length; i++){
			if (arr[i] > smax && arr[i] < max){
				smax = arr[i];
			}
		}
		return smax;

	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		secondMaxEle obj = new secondMaxEle();
		int smax = obj.eleSecondMax(arr);
		System.out.println("Second max element from array is: "+smax);
	}
}
