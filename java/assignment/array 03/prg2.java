/*

Program 2
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465

*/

import java.io.*;



class reverseEle {

	static void revEle(int arr[]) {

		for(int i = 0; i < arr.length; i++){
			int num = arr[i];
			int rev = 0;
			while(num != 0){
				int temp = num % 10;
				rev = (rev * 10) + temp;
				num = num / 10;
				
				
			}
			arr[i] = rev;
			
		}
		System.out.print("Reverse array elemets are: ");
		for(int i = 0; i < arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		revEle(arr);


	}
	
}
	
