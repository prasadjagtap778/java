/*

   Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4

*/

import java.io.*;

class countEle {
	static void eleCount(int arr[]){
		for(int i = 0; i < arr.length; i++){
			int num = arr[i];
			int count = 0;
			while(num != 0){
				num = num / 10;
				count++;
			}
			System.out.print(count+" ");
		}
		System.out.println();
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array: ");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elemnets: ");
		for(int i = 0; i < arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		eleCount(arr);
	}
}
