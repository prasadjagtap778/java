/*
1) Write a program to reverse a String.
input:- Core2web
output: bew2eroC
*/

import java.util.*;

class revStr {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a string: ");
		String str = sc.nextLine();
		int strlength = str.length();
		char ch[] = str.toCharArray();

		char ch1[] = new char[strlength];

		for (int i = strlength-1; i >= 0; i--){
			
			ch1[(strlength-1) - i] = ch[i];
		}

		System.out.println(ch1);
	}
}


