/*
WAP to take size of array from user and also take integer elements from user
find the minimum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: min element = 0
*/

import java.util.Scanner;

class minEle {
	public static void main(String[] args) {
		System.out.println("Enter a size of element: "  );
		Scanner sc = new Scanner(System.in);
		
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements in array: "  );
		
		for(int i  = 0 ; i < size; i++) {
			arr[i] = sc.nextInt();
		
		}
		int min = arr[0];
		for(int i  = 1 ; i < size; i++) {
			if(arr[i] < min) {
				min = arr[i];
			}
		}
		
		
		System.out.println("Minimum element is :"+min);
	}
}


