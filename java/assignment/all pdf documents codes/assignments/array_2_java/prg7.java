/*
Program 7
WAP to find the common elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Common elements :
1
2
*/

import java.util.Scanner;
class commEle {
	public static void main(String[] args) {
		System.out.println("Enter size of array first: ");
		Scanner sc = new Scanner(System.in);
		int size1 = sc.nextInt();
		int arr1[] = new int[size1];

		System.out.println("Enter size of array second: ");
		int size2 = sc.nextInt();
		int arr2[] = new int[size2];
		
		System.out.println("Enter elements of array first: ");
		for(int i = 0; i < size1; i++){
			arr1[i] = sc.nextInt();
		}
		
		System.out.println("Enter elements of array second: ");
		for(int i = 0; i < size2; i++){
			arr2[i] = sc.nextInt();
		}
		System.out.println("Comman Elements are: ");

		for(int i = 0; i < size1; i++) {
			for(int j = 0; j < size2; j++) {
				if(arr1[i] == arr2[j] ){
					System.out.println(arr1[i]);
				}
			}
		
		}
	}
}


			
		

