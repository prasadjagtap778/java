/*
Program 2
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
*/

import java.util.Scanner;
class revDigi {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size of an array: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter array elements : ");

		for(int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("You entered array elemets are : ");
		for(int i = 0; i < size; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		int arr1[] = new int[size];
		System.out.println("reverse elements of array is : ");
		for(int i = 0; i < size; i++) {
			int temp = arr[i];
			int rev = 0;
			while(temp != 0) {
				int rem = temp%10;
				rev  = rev*10 + rem;
				temp = temp/10;
			}
			arr1[i] = rev;
		}
		
		for(int i = 0; i < size; i++) {
			System.out.print(arr1[i]+" ");
		}

	}
}

