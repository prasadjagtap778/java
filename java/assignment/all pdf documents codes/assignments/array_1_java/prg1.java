/*
Program 1
WAP to take size of array from user and also take integer elements from user Print sum
of odd elements only
input : Enter size : 5
Enter array elements : 1 2 3 4 5
output : 9
//1 + 3 + 5
*/

import java.util.Scanner;
class intArr {
	public static void main(String[] args) {
		System.out.println("Enter array size: ");
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		
		int arr[] = new int[size];
		System.out.println("Enter array elements: ");
		int sum = 0;
		for(int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
			if(arr[i] % 2 == 1) {
				sum = sum + arr[i];
			}
		}
		System.out.println("sum of odd elements is = " +sum);
	}
}

