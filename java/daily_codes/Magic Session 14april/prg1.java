/*
Q1
write a program to print the following pattern
D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
USE THIS FOR LOOP STRICTLY for the outer loop
Int row;
Take the number of rows from user
for(int i =1;i<=row;i++){
}
*/

import java.io.*;

class ptr1 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter number of rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 0; i < row ; i++){
			int temp = row;
			int num = 1;
			for(int j = 0 ; j < row; j++){
				if(i%2 == 0){
					System.out.print((char)(64+temp)+""+temp+" ");
					temp--;
				}else{
					System.out.print((char)(64+num)+""+num+" ");
					num++;
				}
			}
			System.out.println();
		}
	}
}
