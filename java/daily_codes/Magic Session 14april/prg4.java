/* 
Q4
WAP to print all even numbers in reverse order and odd numbers in the standard way. Both separately.
Within a range. Take the start and end from user
Input: Enter start number - 2
Enter End number - 9
Output:
8642
3579
*/

import java.io.*;

class ptr4 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter start number: ");
		int start = Integer.parseInt(br.readLine());
		System.out.println("Enter End number: ");
		int end = Integer.parseInt(br.readLine());


		for(int i = end ; i < start; i--){
			if (i %2== 0){
				System.out.println(i);
			}
		}
		System.out.println();
		for(int i = start ; i <= end; i++){
			if(i % 2 == 1){
				System.out.println(i);
			}
		}
	}
}

