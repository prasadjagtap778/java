
import java.util.concurrent.*;

class MyThread implements Runnable {
	int num;
	MyThread(int num) {
		this.num = num;
	}
	public void run() {
		System.out.println(Thread.currentThread() + "Start Thread: " + num);
		dailytask();
		System.out.println(Thread.currentThread() + "End Thread: " + num);
	}

	void dailytask(){
		try {
			Thread.sleep(8000);
		}catch(InterruptedException ie) {
		
		}
	}
}

class ThreadPoolDemo {
	public static void main(String[] args) {
		ExecutorService ser = Executors.newCachedThreadPool();

		for(int i = 0; i <= 8; i++) {
			MyThread obj = new MyThread(i);
			ser.Executor(obj);
		}
		ser.Shutdown();
	}
}

