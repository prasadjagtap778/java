// jagged array through user input using scanner class

import java.util.*;

class jaggArr {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int rows,cols;
		System.out.println("Enter number of rows: ");
		rows = sc.nextInt();

		int arr[][] = new int[rows][];

		for(int i = 0; i < rows; i++){
			System.out.println("Enter numbers of cols in  "+(i+1)+"row");
			cols = sc.nextInt();
			arr[i] = new int[cols];
			
		}
		
		for(int i = 0; i < arr.length;i++){
			System.out.println("Enter elements in"+(i+1)+"row");
			for(int j = 0; j < arr[i].length; j++){
				arr[i][j] = sc.nextInt();
			}
			System.out.println();
		}

		for(int[] x : arr){
			for(int y : x){
				System.out.print(y+" ");
			}
			System.out.println();
		}
	

	}
}
