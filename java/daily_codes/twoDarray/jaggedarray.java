import java.util.*;

class jaggedarray{
	public static void main(String[] args){
		int arr[][] = new int[3][3];
		Scanner sc = new Scanner(System.in);

		arr[0][1] = 10;
		arr[0][2] = 20;
		arr[1][2] = 30;
		arr[2][0] = 40;

		for(int i = 0; i < arr.length; i++){
			for(int j = 0; j < arr[i].length; j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
}

