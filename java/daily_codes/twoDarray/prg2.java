
import java.util.*;


class twoDarray {
	public static void main(String[] args){
		int arr[][] = new int[2][2];
		Scanner sc = new Scanner(System.in);
		for(int i = 0; i < arr.length; i++){
			for(int j = 0 ; j < arr[i].length; j++){
				arr[i][j] = sc.nextInt();
			}
		}

		for(int i = 0; i < arr.length; i++){
			for(int j = 0; j < arr[i].length; j++){
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}

		System.out.println(System.identityHashCode(arr));
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[0][0]));
		System.out.println(System.identityHashCode(arr[0][1]));
		System.out.println(System.identityHashCode(arr[1][0]));
		System.out.println(System.identityHashCode(arr[1][1]));
	}
}
