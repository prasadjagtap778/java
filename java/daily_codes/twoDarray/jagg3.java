// jagged array through user input through BufferedReader

import java.io.*;

class jaggedArr {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());

		int arr[][] = new int[rows][];

		

		for(int i = 0 ; i < arr.length; i++){
			System.out.println("Enter number of colums in "+(i+1)+" row");
			int cols = Integer.parseInt(br.readLine());

			arr[i] = new int[cols];
		}

		for(int i = 0; i < arr.length; i++){
			System.out.println("Enter elements in" +(1 + i)+ "row");
			for(int j = 0; j < arr[i].length; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		for(int[] x: arr){
			for(int y : x){
				System.out.print(y+" ");
			}
			System.out.println();
		}
	}
}
