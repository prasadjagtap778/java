class outer{
	int x = 10;
	int y = 20;
	class inner {
		void fun2() {
			int x = 30;
			int y = 40;
			System.out.println("fun2-Inner");
			//System.out.println(outer.x);
			System.out.println(y);
			fun1();
		}
	}
	void fun1(){
		System.out.println("Fun1-Outer");
	}
}

class client {
	public static void main(String[] args){
		outer obj = new outer();
		obj.fun1();

		outer.inner obj1 = obj.new inner();
		obj1.fun2();
	}
}

