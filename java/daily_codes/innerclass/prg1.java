class outer{
	class inner{
		void fun2(){
			System.out.println("Fun2-Inner");
			//System.out.println(this.this$0);   //Error
		}
	}
	void fun1(){
		System.out.println("Fun1-Outer");
	//	System.out.println(this);
	//	System.out.println(this.this$0); //Error
	}
}
class client {
	public static void main(String[] args){
		outer obj = new outer();
		obj.fun1();
		outer.inner obj1 = obj.new inner();
		obj1.fun2();
	}
}

