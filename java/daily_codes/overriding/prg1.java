class parent {
	int x = 10;
	void m1() {
		System.out.println("In parent m1");
	}
}

class child extends parent {
	int a = 20;
	void m1() {
		System.out.println("In child m1");
	}
}

class Demo {
	Demo(parent p ){
		System.out.println("In constructor parent");
		p.m1();
	}
	Demo (child c){
		System.out.println("In constructor child");
		c.m1();
	}
	public static void main(String[] args) {
		Demo obj = new Demo(new parent());
		Demo obj1 = new Demo(new child());
	}
}

