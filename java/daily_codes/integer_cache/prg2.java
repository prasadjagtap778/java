import java.util.*;

class arrayDemo {
	
	void fun (String arr[]) {
		/*
		arr[1] = "sid";
		arr[2] = "anant";
		*/
	}

	public static void main(String[] args) {
		
		String arr[] = new String[4];
		Scanner sc = new Scanner(System.in);

		for(int i = 0; i < arr.length; i++){
			arr[i] = sc.nextLine();
		}

		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));

		arrayDemo obj = new arrayDemo();
		obj.fun(arr);
		for(String x : arr){
			System.out.println(x);
		}
	

		String x = "sid";
		String y = "anant";

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
                
		/*
		System.out.println("Enter a string: ");
		
		Scanner sc = new Scanner(System.in);
		String str1 = sc.nextLine();
		
		String str2 ;
		
		str2 = sc.nextLine();

		arr[1] = str1;
		arr[2] = str2;
		
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		*/


	}
}

