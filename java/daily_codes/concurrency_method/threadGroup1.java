class MyThread extends Thread {
	MyThread(ThreadGroup tg,String str) {
          super(tg,str);
	}
	
	public void run(){
		
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo {
	public static void main(String[] args) {
		
		//parent Thread Group
		ThreadGroup pthreadGP = new ThreadGroup("Core2Web");
		MyThread obj1 = new MyThread(pthreadGP,"c,cpp,ds");
		MyThread obj2 = new MyThread(pthreadGP,"java");
		MyThread obj3 = new MyThread(pthreadGP,"python");

		//child Thread group
		ThreadGroup cthreadGP = new ThreadGroup(pthreadGP,"Incubater");
		MyThread obj4 = new MyThread(cthreadGP,"flutter");
		MyThread obj5 = new MyThread(cthreadGP,"reactjs");
		MyThread obj6 = new MyThread(cthreadGP,"springboot");

	}
}

		
