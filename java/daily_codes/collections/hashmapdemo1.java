
import java.util.*;

class HashMapDemo {
	public static void main(String[] args) {
		HashMap m = new HashMap();
		m.put("Anant",700);
		m.put("prasad",800);
		m.put("shubham",900);
		m.put("ashish",1000);

		System.out.println(m);

		//System.out.println(m.put("Anant",1000));
		//System.out.println(m);
		//
		Set s = m.keySet();
		System.out.println(s);

		Collection c = m.values();
		System.out.println(c);

		Set s1 = m.entrySet();
		System.out.println(s1);

		Iterator itr = s1.iterator();
		while(itr.hasNext()){

			Map.Entry m1 = (Map.Entry)itr.next();
			System.out.println(m1.getKey()+"...."+m1.getValue());

			if(m1.getKey().equals("Anant")){
				m1.setValue(100000);
			}
		}
		System.out.println(m);
	}
}

