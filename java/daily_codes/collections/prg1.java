import java.util.*;
class ArrayListDemo extends ArrayList{
	public static void main(String[] args) {
		ArrayListDemo al = new ArrayListDemo();
		al.add(10);
		al.add(20.5f);
		al.add("Prasad");
		al.add(true);
		al.add(67.98);
		al.add(999999);
		/*
		System.out.println(al);
		System.out.println(al.size()); // size()
		System.out.println(al.contains(67.98));
		System.out.println(al.indexOf(10));
		System.out.println(al.lastIndexOf(10));
		*/
		/*
		System.out.println(al.get(3));
		System.out.println(al.set(3,"Incubator"));
		al.add(2,"Jagtap");
		*/

		/*
		ArrayListDemo al2 = new ArrayListDemo();
		al2.add(10);
		al2.add(20.5f);
		al2.add("Prasad");
		al2.add(true);
		al2.add(67.98);
		al2.add(999999);
		//System.out.println(al.remove(0));
		System.out.println(al.addAll(al2));

		*/
		System.out.println(al);
		//System.out.println(al2);
		
		Object arr[] = al.toArray();
		System.out.println(arr);

		for(Object data:arr){
			System.out.println(data+" ");
		}
		System.out.println();

	
	}
}


