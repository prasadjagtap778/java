import java.util.*;

class movie implements Comparable{
	String movieName = null;
	float totColl = 0.0f;

	movie(String movieName, float totColl){
		this.movieName = movieName;
		this.totColl = totColl;
	}


	public int compareTo(Object obj1){
		return  -(movieName.compareTo(((movie)obj1).movieName));
	}

	public String toString() {
		return movieName;
	}
}

class TreeDemo {
	public static void main(String[] args) {
		TreeSet ts = new TreeSet();
		ts.add(new movie("Gadar_2",150.00f));
		ts.add(new movie("OMG_2",120.00f));
		ts.add(new movie("jailer",250.00f));

		System.out.println(ts);
	}
}

