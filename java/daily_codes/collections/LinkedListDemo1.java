import java.util.*;

class Building {
	String wing = null;
	int floorNo ;
	Building(String wing, int floorNo) {
		this.wing = wing;
		this.floorNo = floorNo;
	}

	public String toString() {
		return wing+":"+floorNo;
	}
}


class LinkedListDemo {
	public static void main(String[] args) {
		LinkedList ll = new LinkedList();
		
		ll.add(new Building("A",5));
		ll.add(new Building("B",4));
		ll.add(new Building("C",6));

		System.out.println(ll.getFirst());
		System.out.println(ll.getLast());
		System.out.println(ll.removeFirst());
		System.out.println(ll.removeLast());
		ll.addFirst(new Building("D",3));
		ll.addLast(new Building("E",2));
		
		//ll.add(20);
		//ll.addFirst(10);
		//ll.addLast(30);
		System.out.println(ll);
	}
}


