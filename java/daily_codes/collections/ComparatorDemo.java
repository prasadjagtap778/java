import java.util.*;

class Employee {
	String empName = null;
	float sal = 0.0f;

	Employee(String empName, float sal) {
		this.empName = empName;
		this.sal = sal;
	}

	public String toString() {
		return "{"+ empName +":"+sal+"}";
	}
}

class sortName implements Comparator<Employee> {
	public int compare(Employee obj1, Employee obj2){
		return obj1.empName.compareTo(obj2.empName);
	}
}

class sortbysal implements Comparator<Employee> {
       public int compare(Employee obj1, Employee obj2){ 
		return (int)(obj1.sal - obj2.sal);
       }
}

class ListSortDemo {
	public static void main(String[] args) {
		ArrayList<Employee> al = new ArrayList<Employee>();
		al.add(new Employee("Kanha",200000.00f));
		al.add(new Employee("Rahul",250000.00f));
		al.add(new Employee("Ashish",10000.00f));
		al.add(new Employee("Badhe",120000.00f));
		al.add(new Employee("shashi",800000.00f));

		System.out.println(al);
		Collections.sort(al, new sortName());
		System.out.println(al);
		
		Collections.sort(al, new sortbysal());
		System.out.println(al);
	}
}

