class MyThread extern Thread {
	public void run() {
		System.out.println("In main");
		System.out.println(Thread.currentThread().getName());
	}
	public void start() {
		System.out.println("In MyThread start");
		run();
	}
}

class theadDemo{
	public static void main(String[] args) {
		MyThread obj = new MyThread();
		obj.exit();
		obj.start();
		System.out.println(Thread.currentThread().getName());
	}
}
