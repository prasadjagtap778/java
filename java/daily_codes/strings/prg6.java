class stringDemo {
	public static void main(String[] args) {
		String str1 = "prasad";
		String str2 = "jagtap";
		System.out.println(str1 + str2);
		
		String str3 = "prasadagtap";
		String str4 = str1 + str2;
		String str5 = new String("prasadjagtap");
		
		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
		System.out.println(str5.hashCode());
	}
}
