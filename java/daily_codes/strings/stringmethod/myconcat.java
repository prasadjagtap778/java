
import java.util.*;

class myconcat {
	static void concatstr(String str1, String str2){
		int arrlength = str1.length() + str2.length();
		char str3arr[] = new char[arrlength];
		char str1arr[] = str1.toCharArray();
		char str2arr[] = str2.toCharArray();

		for(int i = 0; i < arrlength; i++){
			if (i < str1.length()){
				str3arr[i] = str1arr[i];
			}else{
				str3arr[i] = str2arr[i-str1.length()];
			}
		}
		
		System.out.print("Concatinated String is : ");
		
		for(int i = 0; i < arrlength ; i++){
			System.out.print(str3arr[i]);
		}
		System.out.print("\n");

	}


	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a first string: ");
		String str1 = sc.nextLine();
		
		System.out.println("Enter a second string: ");
		String str2 = sc.nextLine();

		concatstr(str1,str2);
	}
}


