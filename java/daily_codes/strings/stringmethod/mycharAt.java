
import java.util.*;

class mycharAt {
	
	static char myCharArr(String str){
		Scanner sc = new Scanner(System.in);
		
		int strlength = str.length();
		char strarr[] = str.toCharArray();
		
		System.out.println("Enter a index to find character: ");
		int index = sc.nextInt();
		
		return strarr[index];
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string: ");
		String str = sc.nextLine();
		
		char ch = myCharArr(str);
		
		System.out.println(ch);	
	}
}

