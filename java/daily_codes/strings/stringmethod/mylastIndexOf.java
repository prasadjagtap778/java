import java.util.*;

class mylastIndexOf {
	static int mylastIndexOfDemo(String str, String find, int index ){
		char strarr[] = str.toCharArray();
		char findarr[] = find.toCharArray();

		for(int i = 0; i < index; i++){
			int findindex = i;
			for(int j = 0; j < find.length(); j++){
				if(strarr[i] == findarr[j]){
					i++;
					if(j == (find.length()-1)){
						return findindex;
					}
				}
			}
		}
		return -1;
	}



	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string: ");
		String str = sc.nextLine();
		
		System.out.println("Enter a word or character to find the last index: ");
		String find = sc.nextLine();

		System.out.println("Enter a index to search upto that index: ");
		int index = sc.nextInt();

		int findindex = mylastIndexOfDemo(str,find,index);
		if(findindex == -1){
			System.out.println("Not found");
		}else{
			System.out.println("Found at index: "+findindex);
		}
	}
}


		



