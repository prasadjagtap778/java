
import java.util.*;

class myindexOf {

	static int myindexOf(char ch, String str){
		char arr[] = str.toCharArray();

		for(int i = 0; i < str.length(); i++){
			if(arr[i] == ch){
				return i;
			}else{
				return -1;
			}
		}
		return -1;
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string : ");
		String str = sc.nextLine();
		
		char ch = sc.next().charAt(0);

		System.out.println("Enter a charcater to find in a string: ");
		int index = myindexOf(ch,str);
		if(index == -1){
			System.out.println("Character not found");
		}else{
			System.out.println("character found at index: "+index);
		}

	}
}


