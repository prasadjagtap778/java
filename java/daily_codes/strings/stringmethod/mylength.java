import java.util.*;


class myLengthDemo {

	static int myLength(String str){
		char ch[] = str.toCharArray();
		int count = 0;
		for(int i = 0; i < ch.length; i++){
			count++;
		}
		return count;
	}



	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a first string: ");
		String str1 = sc.next();
		System.out.println("Enter a Second string: ");
		String str2 = sc.next();
		
		int length1 = myLength(str1);
		int length2 = myLength(str2);

		System.out.println("Length of first string is: "+length1);
		System.out.println("Length of second string is: "+length2);
	}
}


		
