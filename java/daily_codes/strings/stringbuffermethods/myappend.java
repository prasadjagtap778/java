
import java.util.*;

class myAppend {
	static StringBuffer myappendDemo(StringBuffer sb, StringBuffer sb1){
		
		String sbstr = sb.toString();
		String sb1str = sb1.toString();

		char arr1[] = sbstr.toCharArray();
		char arr2[] = sb1str.toCharArray();
		
		int newlength =sbstr.length() + sb1str.length();
		char arr3[] = new char[newlength];

		for(int i = 0; i < newlength; i++){
			if(i < sbstr.length()){
				arr3[i] = arr1[i];
			}else{
				arr3[i] = arr2[i-sbstr.length()];
			}
		}
		sbstr = new String(arr3);
		sb = new StringBuffer(sbstr);
		return sb;

	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string: ");
		StringBuffer sb = new StringBuffer(sc.nextLine());

		System.out.println("Enter a string to append: ");
		StringBuffer sb1 = new StringBuffer(sc.nextLine());

		sb = myappendDemo(sb,sb1);
		System.out.println(sb);

	}
}

