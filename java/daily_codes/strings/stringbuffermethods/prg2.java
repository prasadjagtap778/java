class stringBfDemo {
	public static void main(String[] args){
		StringBuffer str1 = new StringBuffer();
		System.out.println("Before appending capacity of stringBuffer "+str1.capacity());
		
		StringBuffer str2 = str1.append("prasad");
		str2 = str1.append("prasad");
		str2 = str1.append("prasad");

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(str2);

		System.out.println(str1.capacity());
		
	}
}
