
import java.util.*;

class insertDemo {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string: ");
		StringBuffer sb = new StringBuffer(sc.nextLine());
	        System.out.println(System.identityHashCode(sb)); 	
		System.out.println("Enter a index and to append in a string: ");
		int index = sc.nextInt();
		sc.skip("\n");
		System.out.println("Enter a string to append : ");
				
		String str = sc.nextLine();
		
		sb.insert(index,str);
		
	        System.out.println(System.identityHashCode(sb)); 	
		System.out.println(sb);
	}
}
