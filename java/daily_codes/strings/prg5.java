class stringDemo {
	public static void main(String[] args) {
		String str1 = "Kanha";
		String str2 = str1;
		String str3 = new String(str2);

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
	}
}
